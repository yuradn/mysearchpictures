package com.example.yura.mysearchpictures.Model;

import java.io.Serializable;

public class Picture implements Serializable {

    private Long id;
    private String url;
    private String title;

    public Picture() {};

    public Picture (Long id, String url, String title){
        this.id    = id;
        this.url   = url;
        this.title = title;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getUrl() {
        return url;
    }

    public void setUrl(String url) {
        this.url = url;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    @Override
    public String toString() {
        return "Picture{" +
                "id=" + id +
                ", url='" + url + '\'' +
                ", title='" + title + '\'' +
                '}';
    }
}
