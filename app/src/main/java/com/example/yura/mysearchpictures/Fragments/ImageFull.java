package com.example.yura.mysearchpictures.Fragments;


import android.app.DialogFragment;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;

import com.example.yura.mysearchpictures.Adapter.ImageAdapter;
import com.example.yura.mysearchpictures.MainActivity;
import com.example.yura.mysearchpictures.R;
import com.nostra13.universalimageloader.core.ImageLoader;
import com.nostra13.universalimageloader.core.ImageLoaderConfiguration;

public class ImageFull extends DialogFragment{

    String LOG_TAG = "DialogFragment";
    String url;

    public ImageFull() {}

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View v = inflater.inflate(R.layout.full_image, null);

        if (getArguments()!=null) {
            url = (String) getArguments().getSerializable("url");
        } else {
            Log.d(LOG_TAG,"arguments is null!");
        }

        ImageView imageView = (ImageView) v.findViewById(R.id.imageFullView);

        ImageLoader imageLoader = MainActivity.imageLoader.getInstance();
        //imageLoader.init(ImageLoaderConfiguration.createDefault(getActivity()));
        imageLoader.displayImage(url, imageView); // Запустили асинхронный показ картинки

        return v;
    }
}
