package com.example.yura.mysearchpictures;

import android.app.Activity;
import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import android.util.Log;

import com.example.yura.mysearchpictures.Model.Picture;

import java.util.ArrayList;

public class MyDb {

    MyDb myDb;
    DBHelper dbHelper;
    String LOG_TAG = "MyDb";
    Activity activity;

    //public MyDb() {};

    public MyDb(Activity activity){
        this.activity = activity;
        dbHelper = new DBHelper(activity);
    }

//    public MyDb getInstance() {
//        if (myDb==null) myDb=new MyDb();
//        return myDb;
//    }

    public long addNewUrl (String url, String title) {
        // создаем объект для данных
        ContentValues cv = new ContentValues();
        // подключаемся к БД
        SQLiteDatabase db = dbHelper.getWritableDatabase();
        // подготовим данные для вставки в виде пар: наименование столбца - значение
        cv.put("url",   url  );
        cv.put("title", title);
        long rowID = db.insert("mytable", null, cv);
        Log.d(LOG_TAG, "row inserted, ID = " + rowID);
        return rowID;
    }

    public void readAll() {
        Log.d(LOG_TAG, "--- Rows in mytable: ---");

        MainActivity.pictures = new ArrayList<Picture>();

        // подключаемся к БД
        SQLiteDatabase db = dbHelper.getWritableDatabase();
        // делаем запрос всех данных из таблицы mytable, получаем Cursor
        Cursor c = db.query("mytable", null, null, null, null, null, null);

        // ставим позицию курсора на первую строку выборки
        // если в выборке нет строк, вернется false
        if (c.moveToFirst()) {

            // определяем номера столбцов по имени в выборке
            int idColIndex = c.getColumnIndex("id");
            int urlColIndex = c.getColumnIndex("url");
            int titleColIndex = c.getColumnIndex("title");

            do {
                Picture picture = new Picture(c.getLong(idColIndex),
                        c.getString(urlColIndex), c.getString(titleColIndex));
                MainActivity.pictures.add(picture);
                // получаем значения по номерам столбцов и пишем все в лог
                Log.d(LOG_TAG,
                        "ID = " + picture.getId() +
                                ", url = " + picture.getUrl() +
                                ", title = " + picture.getTitle());
                // переход на следующую строку
                // а если следующей нет (текущая - последняя), то false - выходим из цикла
            } while (c.moveToNext());
    }

    }

    public void delete(Long id) {
        // подключаемся к БД
        SQLiteDatabase db = dbHelper.getWritableDatabase();
        // удаляем по id
        int delCount = db.delete("mytable", "id = " + id, null);
        Log.d(LOG_TAG, "deleted rows count = " + delCount);
    }

    class DBHelper extends SQLiteOpenHelper {

        public DBHelper(Context context) {
            // конструктор суперкласса
            super(context, "myDB", null, 1);
        }

        @Override
        public void onCreate(SQLiteDatabase db) {
            Log.d(LOG_TAG, "--- onCreate database ---");
            // создаем таблицу с полями
            db.execSQL("create table mytable ("
                    + "id long primary key autoincrement,"
                    + "url text,"
                    + "title text" + ");");
        }

        @Override
        public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {

        }
    }

}
