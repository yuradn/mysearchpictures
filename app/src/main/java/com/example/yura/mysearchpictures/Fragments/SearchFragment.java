package com.example.yura.mysearchpictures.Fragments;

import android.content.Context;
import android.graphics.Color;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AbsListView;
import android.widget.ListAdapter;
import android.widget.ListView;

import com.actionbarsherlock.app.SherlockFragment;
import com.example.yura.mysearchpictures.GoogleSearch;
import com.example.yura.mysearchpictures.MainActivity;
import com.example.yura.mysearchpictures.R;

public class SearchFragment extends SherlockFragment {

    private String LOG_TAG="SearchFragment";
    private ListView listView;
    private Context context;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_search_page, null);

        context = getSherlockActivity().getApplicationContext();

        listView = (ListView) view.findViewById(R.id.listView);
        listView.setOnScrollListener(new AbsListView.OnScrollListener() {
            @Override
            public void onScrollStateChanged(AbsListView view, int scrollState) {

            }

            @Override
            public void onScroll(AbsListView view, int firstVisibleItem, int visibleItemCount, int totalItemCount) {


                if (firstVisibleItem==0&&visibleItemCount==0&&totalItemCount==0) return;
                if (firstVisibleItem+visibleItemCount==totalItemCount) {
                    Log.d("ListView","firstVisibleItem: "+firstVisibleItem);
                    Log.d("ListView","visibleItemCount: "+visibleItemCount);
                    Log.d("ListView","totalItemCount: "+totalItemCount);

                    if (MainActivity.googleSearch && MainActivity.internetPictures.size()<64) {
                        MainActivity.googleSearch=false;
                        Log.d(LOG_TAG,"********** MainActivity.googleSearch");
                        if (!MainActivity.googleSearch) new GoogleSearch().startSearch(false);
                    }
                    else {
                        Log.d(LOG_TAG,"********** MainActivity.googleSearch=false");
                    }

                }
            }
        });

        view.setBackgroundColor(Color.LTGRAY);

        return view;
    }

    public ListView getListView() {
        return listView;
    }

    public void setListView1(ListView listView) {
        this.listView = listView;
    }

    public Context getContext() {
        return context;
    }
}

