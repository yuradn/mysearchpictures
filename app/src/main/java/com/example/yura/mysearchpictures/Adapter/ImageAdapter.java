package com.example.yura.mysearchpictures.Adapter;

import android.content.Context;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.CheckBox;
import android.widget.ImageView;
import android.widget.TextView;

import com.example.yura.mysearchpictures.Fragments.ImageFull;
import com.example.yura.mysearchpictures.MainActivity;
import com.example.yura.mysearchpictures.Model.Picture;
import com.example.yura.mysearchpictures.MyDb;
import com.example.yura.mysearchpictures.R;
import com.google.api.services.customsearch.model.Result;
import com.nostra13.universalimageloader.core.DisplayImageOptions;
import com.nostra13.universalimageloader.core.ImageLoader;
import com.nostra13.universalimageloader.core.ImageLoaderConfiguration;
import com.nostra13.universalimageloader.core.assist.ImageScaleType;

import java.util.List;

public class ImageAdapter extends BaseAdapter {

    Context context;
    LayoutInflater lInflater;
    ImageLoader imageLoader;

    public ImageAdapter () {
        this.context = MainActivity.searchFragment.getContext();
        lInflater = (LayoutInflater) context
                .getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        imageLoader = MainActivity.imageLoader.getInstance();
        //imageLoader.init(ImageLoaderConfiguration.createDefault(context)); // Проинициализировали конфигом по умолчанию

    }

    @Override
    public int getCount() {
        return MainActivity.internetPictures.size();
    }

    @Override
    public Object getItem(int position) {
        return MainActivity.internetPictures.get(position);
    }

    @Override
    public long getItemId(int position) {
        return MainActivity.internetPictures.get(position).getId();
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        View view = convertView;
        if (view == null) {
            view = lInflater.inflate(R.layout.list_new_image, parent, false);
        }

        final Picture picture = MainActivity.internetPictures.get(position);

        TextView txtId = (TextView) view.findViewById(R.id.txtId);
        txtId.setText(Integer.toString(position+1));

        ImageView imageView = (ImageView) view.findViewById(R.id.smallImage);
        imageView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                ImageFull imageFull = new ImageFull();
                Bundle bundle = new Bundle();
                bundle.putSerializable("url", picture.getUrl());
                imageFull.setArguments(bundle);
                imageFull.show(MainActivity.activity.getFragmentManager(), "dialog");
            }
        });


        final String imageUrl = picture.getUrl();

        DisplayImageOptions options = new DisplayImageOptions.Builder()
                .cacheOnDisk(true)
                .cacheInMemory(true)
                .showImageForEmptyUri(R.drawable.abs__ab_solid_shadow_holo)
                .showImageOnLoading(R.drawable.abs__ic_search)
                .resetViewBeforeLoading(false)
                .imageScaleType(ImageScaleType.IN_SAMPLE_POWER_OF_2)
                .build();

        //imageView.setImageBitmap(imageLoader.loadImageSync(imageUrl));
        //Log.d("imageView",imageView.)
        imageLoader.displayImage(imageUrl, imageView, options); // Запустили асинхронный показ картинки

        final CheckBox addImage = (CheckBox) view.findViewById(R.id.checkBox);
        addImage.setText(picture.getTitle());
        addImage.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Long rowId = new MyDb(MainActivity.activity).addNewUrl(imageUrl,picture.getTitle());
                Picture pictureSaved = new Picture(rowId, imageUrl, picture.getTitle());
                MainActivity.savedPictures.add(pictureSaved);
                //MainActivity.imagePageFragment.getListView().invalidateViews();
                addImage.setClickable(false);

                //items.remove(result);
                //MainActivity.searchFragment.getListView().invalidateViews();

            }
        });

        return view;
    }
}
