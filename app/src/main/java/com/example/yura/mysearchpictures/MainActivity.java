package com.example.yura.mysearchpictures;

import android.app.Activity;
import android.content.Context;
import android.graphics.Bitmap;
import android.os.Bundle;
import android.os.Handler;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;
import android.support.v4.view.PagerAdapter;
import android.support.v4.view.ViewPager;
import android.util.Log;
import android.view.View;
import android.view.WindowInsets;
import android.view.inputmethod.InputMethodManager;
import android.widget.ImageView;
import android.widget.Toast;
import com.actionbarsherlock.app.SherlockFragmentActivity;
import com.actionbarsherlock.view.Menu;
import com.actionbarsherlock.view.MenuItem;
import com.actionbarsherlock.widget.SearchView;
import com.example.yura.mysearchpictures.Adapter.DbImageAdapter;
import com.example.yura.mysearchpictures.Adapter.ImageAdapter;
import com.example.yura.mysearchpictures.Fragments.ImagePageFragment;
import com.example.yura.mysearchpictures.Fragments.SearchFragment;
import com.example.yura.mysearchpictures.Model.Picture;
import com.google.api.services.customsearch.Customsearch;
import com.google.api.services.customsearch.model.Result;
import com.nostra13.universalimageloader.cache.disc.DiskCache;
import com.nostra13.universalimageloader.cache.disc.impl.UnlimitedDiscCache;
import com.nostra13.universalimageloader.cache.memory.impl.UsingFreqLimitedMemoryCache;
import com.nostra13.universalimageloader.core.DisplayImageOptions;
import com.nostra13.universalimageloader.core.ImageLoader;
import com.nostra13.universalimageloader.core.ImageLoaderConfiguration;
import com.nostra13.universalimageloader.core.assist.ImageScaleType;
import com.nostra13.universalimageloader.core.download.BaseImageDownloader;
import com.nostra13.universalimageloader.utils.StorageUtils;

import java.io.File;
import java.util.ArrayList;
import java.util.LinkedList;
import java.util.List;

public class MainActivity extends SherlockFragmentActivity {

    String LOG_TAG = "MainActivity";

    // Collection of Picture from result internet search
    public static LinkedList<Picture> internetPictures = new LinkedList<Picture>();
    // Collection of Picture from DB
    public static LinkedList<Picture> savedPictures    = new LinkedList<Picture>();
    // Word from search
    public static String searchQuery = "";
    // My ImageLoader
    public static ImageLoader imageLoader;
    // My ImageAdapter
    public static ImageAdapter imageAdapter;

    Boolean init = true;

    SearchView searchView;

    ViewPager pager;
    PagerAdapter pagerAdapter;
    int currentPage=0;

    public static volatile boolean googleSearch=false;

    Menu myMenu;
    MenuItem searchMenu;
    Boolean searchviewFocus=false;

    public static int startIndex = 0;


    public static ImagePageFragment imagePageFragment;
    public static SearchFragment    searchFragment;

    public static MyDb myDb;

    public static Activity activity;

    public static ArrayList<Picture> pictures = null;

    public static LinkedList<Picture> searchList;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        init();

        imageLoader = ImageLoader.getInstance(); // Получили экземпляр
        File cacheDir = StorageUtils.getCacheDirectory(getApplicationContext(), true);
        ImageLoaderConfiguration config = new ImageLoaderConfiguration.Builder(getApplicationContext())
                .threadPoolSize(5)
                .memoryCacheExtraOptions(60,80)
                .threadPriority(Thread.MIN_PRIORITY + 2)
                .diskCache(new UnlimitedDiscCache(cacheDir))
                .build();



        imageLoader.init(config);

        searchList = new LinkedList<Picture>();

        pictures = new ArrayList<Picture>();

        activity = this;
        myDb = new MyDb(this);
        myDb.readAll();

        pager = (ViewPager) findViewById(R.id.pager);
        pagerAdapter = new MyFragmentPagerAdapter(getSupportFragmentManager());
        pager.setAdapter(pagerAdapter);

        pager.setOnPageChangeListener(new ViewPager.OnPageChangeListener() {
            @Override
            public void onPageScrolled(int position, float positionOffset, int positionOffsetPixels) {
            }
            @Override
            public void onPageSelected(int position) {
                currentPage=position;
                switch (position) {
                    case 0:
                        //getSupportActionBar().show();
                        searchMenu.setVisible(true);
                        if (searchviewFocus) {
                            searchView.findFocus();
                            searchviewFocus=false;
                        }
                        searchView.setVisibility(View.VISIBLE);

                        break;
                    case 1:
                        //getSupportActionBar().hide();

                        searchMenu.setVisible(false);

                        if (searchView.isFocusable()) {
                            searchView.clearFocus();
                            searchviewFocus=true;
                        }
                        searchView.setVisibility(View.INVISIBLE);

                        //rebootDbList();

                        if (init) {

                            DbImageAdapter dbImageAdapter = new DbImageAdapter(imagePageFragment.getContext(),
                                    pictures);

                            imagePageFragment.getListView().setAdapter(dbImageAdapter);
                            init = false;
                        }
                        else {
                            imagePageFragment.getListView().invalidateViews();
                        }

                        break;
                }
                Log.d(LOG_TAG,"pager, position = "+position);
            }
            @Override
            public void onPageScrollStateChanged(int state) {
            }
        });





//        DbImageAdapter dbImageAdapter = new DbImageAdapter(imagePageFragment.getContext(),
//                pictures);
//
//        imagePageFragment.getListView().setAdapter(dbImageAdapter);

    }

    private class MyFragmentPagerAdapter extends FragmentPagerAdapter {

        public MyFragmentPagerAdapter(FragmentManager fm) {
            super(fm);
        }

        @Override
        public Fragment getItem(int position) {
            android.support.v4.app.Fragment current = null;
            currentPage = position;
            Log.d(LOG_TAG,"Current page="+currentPage);

            switch (position) {
                case 0:
                    searchFragment = new SearchFragment();
                    current = searchFragment;
                    break;
                case 1:
                    imagePageFragment = new ImagePageFragment();

//                    DbImageAdapter dbImageAdapter = new DbImageAdapter(imagePageFragment.getContext(),
//                            pictures);
//
//                    imagePageFragment.getListView().setAdapter(dbImageAdapter);

                    current = imagePageFragment;
                    break;
            }
            return current;
        }

        @Override
        public CharSequence getPageTitle(int position) {
            switch (position){
                case 0: return "Search";
                case 1: return "Saved";
            }
            return Integer.toString(position);
        }

        @Override
        public int getCount() {
            return 2;
        }

    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        //Create the search view
        Log.d(LOG_TAG,"onCreateOptionsMenu(Menu menu)");

        searchView = new SearchView(getSupportActionBar().getThemedContext());
        searchView.setQueryHint("Search");
        myMenu = menu;

        menu.add(Menu.NONE,Menu.NONE,1,"Search")
                .setIcon(R.drawable.abs__ic_search)
                .setActionView(searchView)
                .setShowAsAction(MenuItem.SHOW_AS_ACTION_ALWAYS
                        |
                        MenuItem.SHOW_AS_ACTION_COLLAPSE_ACTION_VIEW );

        searchMenu = menu.getItem(0);

        searchView.setOnQueryTextListener(new SearchView.OnQueryTextListener() {
            @Override
            public boolean onQueryTextChange(String newText) {
                if (newText.length() > 0) {
                    // Search
                    Log.d("onQueryTextChange", "New Text > 0 : " + newText);

                } else {
                    // Do something when there's no input
                    Log.d("onQueryTextChange","New Text else : ");
                }
                return false;
            }

            @Override
            public boolean onQueryTextSubmit(String query) {

                InputMethodManager imm = (InputMethodManager) getBaseContext().getSystemService(Context.INPUT_METHOD_SERVICE);
                imm.hideSoftInputFromWindow(searchView.getWindowToken(), 0);

                init();

                searchQuery=query;
                Toast.makeText(getBaseContext(), "Search: " + query, Toast.LENGTH_SHORT).show();

                //searchList = googleSearch.newSearch(query, searchFragment);
                //if (searchList==null) return false;

                new GoogleSearch().startSearch(true);

                //setSupportProgressBarIndeterminateVisibility(false);

                Handler handler = new Handler();
                handler.postDelayed(new Runnable() {
                    public void run() {
                        setSupportProgressBarIndeterminateVisibility(true);
                        Toast.makeText(getBaseContext(), "IndeterminateVisibility", Toast.LENGTH_SHORT).show();
                    }
                }, 2000);

                return false; }
        });

        return true;
    }

    public void rebootDbList() {
        myDb.readAll();
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        Log.d(LOG_TAG,"onOptionsItemSelected(MenuItem item)");

        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            return true;
        }

        return super.onOptionsItemSelected(item);
    }

    public void init(){
        MainActivity.internetPictures.clear();
        MainActivity.startIndex=0;
        init = true;
        googleSearch=false;

    }

    public MyDb getMyDb() {
        return myDb;
    }
}
