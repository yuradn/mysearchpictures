package com.example.yura.mysearchpictures.Adapter;

import android.content.Context;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;
import com.example.yura.mysearchpictures.Fragments.ImageFull;
import com.example.yura.mysearchpictures.MainActivity;
import com.example.yura.mysearchpictures.Model.Picture;
import com.example.yura.mysearchpictures.R;
import com.nostra13.universalimageloader.core.ImageLoader;
import com.nostra13.universalimageloader.core.ImageLoaderConfiguration;
import java.util.ArrayList;

public class DbImageAdapter extends BaseAdapter {

    ArrayList<Picture>  pictures;
    Context             context;
    LayoutInflater      lInflater;
    ImageLoader         imageLoader;

    public DbImageAdapter (Context context, ArrayList<Picture> pictures) {
        this.pictures = pictures;
        this.context = context;
        lInflater = (LayoutInflater) context
                .getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        imageLoader = ImageLoader.getInstance();
        imageLoader.init(ImageLoaderConfiguration.createDefault(context)); // Проинициализировали конфигом по умолчанию
    }

    @Override
    public int getCount() {
        return pictures.size();
    }

    @Override
    public Object getItem(int position) {
        return pictures.get(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        View view = convertView;
        if (view == null) {
            view = lInflater.inflate(R.layout.list_db_image, parent, false);
        }

        final Picture picture = pictures.get(position);

        ImageView imageView = (ImageView) view.findViewById(R.id.smallImage);
        imageView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                ImageFull imageFull = new ImageFull();
                Bundle bundle = new Bundle();
                bundle.putSerializable("url", picture.getUrl());
                imageFull.setArguments(bundle);
                imageFull.show(MainActivity.activity.getFragmentManager(), "dialog");
            }
        });


        String imageUrl = picture.getUrl();

        imageLoader.displayImage(imageUrl, imageView); // Запустили асинхронный показ картинки

        TextView textView = (TextView) view.findViewById(R.id.textView);
        textView.setText(picture.getTitle());

        Button butDelete = (Button) view.findViewById(R.id.butDelete);
        butDelete.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                MainActivity.myDb.delete(picture.getId());
                MainActivity.pictures.remove(picture);
//                DbImageAdapter dbImageAdapter = new DbImageAdapter(MainActivity.imagePageFragment.getContext(),
//                        pictures);
//                MainActivity.imagePageFragment.getListView().setAdapter(dbImageAdapter);
                MainActivity.imagePageFragment.getListView().invalidateViews();
            }
        });

        return view;
    }

}
