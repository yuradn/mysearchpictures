package com.example.yura.mysearchpictures;

import android.app.ProgressDialog;
import android.os.AsyncTask;
import android.util.Log;

import com.example.yura.mysearchpictures.Adapter.ImageAdapter;
import com.example.yura.mysearchpictures.Fragments.SearchFragment;
import com.example.yura.mysearchpictures.Model.Picture;
import com.google.api.services.customsearch.model.Result;
import org.htmlcleaner.HtmlCleaner;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.MalformedURLException;
import java.net.URL;
import java.net.URLConnection;
import java.net.URLEncoder;
import java.util.LinkedList;
import java.util.List;

public class GoogleSearch {



    private static final String API_KEY = "AIzaSyCfTHT2_flB-zuicapkMSGCQMSW5FQbLXM";
    private static final String engine = ("006362779814540710079:vbyugayezxk");
    private String LOG_TAG = "GoogleSearch";
    private boolean nextSearch = false;

    JSONObject json;

    private List<Result> searchList = null;

    private LinkedList<Picture> pictures;

    SearchFragment searchFragment;

    ProgressDialog pd;

    public GoogleSearch(){}

    // if nextSearch=true to two search in one run
    public void startSearch(boolean nextSearch){
        //this.searchFragment = searchFragment;
        if (MainActivity.googleSearch) return;
        else Log.d(LOG_TAG,"********** MainActivity.googleSearch=false");
        this.nextSearch = nextSearch;
        new JsonSearchTask().execute();
    }

//    public void nextSearch(String searchString, SearchFragment searchFragment, boolean nextSearch){
//        new JsonSearchTask(searchString).execute();
//        this.searchFragment = searchFragment;
//    }

    private class JsonSearchTask extends AsyncTask<Void, Void, Void> {

        private String sq;
        private List<Result> items;

        String searchResult = "";
        String search_url1 = "https://ajax.googleapis.com/ajax/services/search/images?v=1.0&start=";
        String search_url2 = "&rsz=";
        String search_url3 = "&q=";
        String search_query;

        JsonSearchTask(){}

        @Override
        protected Void doInBackground(Void... arg0) {

            try {

                sq = URLEncoder.encode(MainActivity.searchQuery, "utf-8");

                if (nextSearch)
                do {

                    String startIndex = Integer.toString(MainActivity.startIndex);

                    search_query = search_url1 + startIndex + search_url2 + "8" +
                            search_url3+sq;

                    LinkedList<Picture> pictList = new LinkedList<Picture>();

                    pictList = ParseResult(sendQuery(search_query));

                    MainActivity.startIndex+=8;

                    MainActivity.internetPictures.addAll(pictList);

                } while (MainActivity.internetPictures.size()<15);
                else {
                    String startIndex = Integer.toString(MainActivity.startIndex);

                    // step for add picture
                    int step = 1;

                    search_query = search_url1 + startIndex + search_url2 + Integer.toString(step) +
                            search_url3+sq;

                    LinkedList<Picture> pictList = new LinkedList<Picture>();

                    pictList = ParseResult(sendQuery(search_query));

                    MainActivity.startIndex+=step;

                    MainActivity.internetPictures.addAll(pictList);
                }




            } catch (MalformedURLException e) {
                e.printStackTrace();
            } catch (IOException e) {
                e.printStackTrace();
            } catch (JSONException e) {
                e.printStackTrace();
            }

            /*
            JsonFactory jsonFactory = new JacksonFactory();

            //searchResult = ParseResult(sendQuery(search_query));

            //HttpTransport httpTransport = new ApacheHttpTransport(;//HttpTransport();

            Customsearch customsearch = new Customsearch(new NetHttpTransport(),
                    jsonFactory,null);


            try {
                Customsearch.Cse cse = customsearch.cse();
                Customsearch.Cse.List list = cse.list(sq);

                list.setKey(API_KEY);
                list.setCx(engine);
                list.setSearchType("image");
                //list.setNum((long) 25);


                if (nextSearch) list.setStart(MainActivity.startIndex);

                Search results = list.execute();

                items = results.getItems();

                if (items!=null) {
                    Log.d(LOG_TAG,"Size of items: "+items.size());
                    Log.d(LOG_TAG,"Total results: "+results.getSearchInformation().getTotalResults());
                    Log.d(LOG_TAG,"FormattedTotalResults: "+results.getSearchInformation().getFormattedTotalResults());
                    Log.d(LOG_TAG,""+results.getQueries().get("nextPage").get(0).getStartIndex());

                    //System.out.println("Start index: "+results.getSearchInformation().get("startIndex"));
                for(Result result:items)
                {
                    Log.d(LOG_TAG,"Title: "+result.getHtmlTitle());
                    Log.d(LOG_TAG, result.getLink());
                }
                }
                else {
                    items = new LinkedList<Result>();
                }


            } catch (IOException e) {
                e.printStackTrace();
            }
            */
            items = new LinkedList<Result>();

            return null;
        }

        @Override
        protected void onPreExecute() {
            //buttonSearch.setEnabled(false);
            //buttonSearch.setText("Wait...");

            pd = new ProgressDialog(MainActivity.activity);
            pd.setIndeterminate(false);
            if (MainActivity.internetPictures.size()<16) pd.show();

            super.onPreExecute();
        }

        @Override
        protected void onPostExecute(Void result) {

            Log.d(LOG_TAG,"OnPostExecute");

            if (pd.isShowing()) pd.dismiss();

            for (int i=0;i<MainActivity.internetPictures.size();i++) {
                Picture picture = MainActivity.internetPictures.get(i);
                Log.d(LOG_TAG,i+": "+picture.getId()+" "+picture.getTitle()+" "+picture.getUrl());
            }

            Log.d("ListView", "****************************************************************");
            if (MainActivity.internetPictures.size()<=16) {
                MainActivity.imageAdapter = new ImageAdapter();
                MainActivity.searchFragment.getListView().setAdapter(MainActivity.imageAdapter);
            }
            else
            if (!MainActivity.googleSearch)
            {
//                MainActivity.imageAdapter.notifyDataSetChanged();
//                MainActivity.searchFragment.getListView().invalidateViews();
//                MainActivity.searchFragment.getListView().refreshDrawableState();
                Runnable run = new Runnable(){
                    public void run(){
                        //reload content
                        MainActivity.imageAdapter.notifyDataSetChanged();
                        MainActivity.searchFragment.getListView().invalidateViews();
                        MainActivity.searchFragment.getListView().refreshDrawableState();
                    }
                };
                run.run();
                //MainActivity.searchFragment.getListView().getAdapter().no
                //MainActivity.searchFragment.getListView().invalidateViews();
                //MainActivity.searchFragment.getListView().notifyAll();
            }
                //listImages = getImageList(resultArray);
                //SetListViewAdapter(listImages);

            /*
//            webView.loadData(searchResult,
//                    "text/html",
//                    "UTF-8");

            //buttonSearch.setEnabled(true);
            //buttonSearch.setText("Search");
            if (nextSearch) {
                searchList=MainActivity.searchList;
                searchList.addAll(items);
                searchFragment.getListView().invalidateViews();
            }
            else {
                searchList = items;
                if (searchList == null) searchList = new LinkedList<Result>();
                Log.d("ListView", "****************************************************************");
                ImageAdapter imageAdapter = new ImageAdapter(searchFragment.getContext(), searchList);
                searchFragment.getListView().setAdapter(imageAdapter);
                //Log.d("ListView", Integer.toString(searchFragment.getListView().getMaxScrollAmount()));
                //Log.d("ListView", Integer.toString(searchFragment.getListView().get);
            }

            MainActivity.searchList = searchList;
            MainActivity.googleSearch = false;
            */
            MainActivity.googleSearch = true;
            super.onPostExecute(result);
        }

        private JSONObject sendQuery(String query) throws IOException{


            JSONObject jsonObject = new JSONObject();

            URL searchURL = new URL(query);

            URLConnection connection = searchURL.openConnection();

            String line;
            StringBuilder builder = new StringBuilder();
            BufferedReader reader = new BufferedReader(new InputStreamReader(connection.getInputStream()));
            while((line = reader.readLine()) != null) {
                builder.append(line);
            }

            Log.d(LOG_TAG,"Builder string => "+builder.toString());

            try {
                jsonObject = new JSONObject(builder.toString());
            } catch (JSONException e) {
                e.printStackTrace();
            }

/*
            if(httpURLConnection.getResponseCode() == HttpURLConnection.HTTP_OK){
                InputStreamReader inputStreamReader = new InputStreamReader(httpURLConnection.getInputStream());
                BufferedReader bufferedReader = new BufferedReader(
                        inputStreamReader,
                        8192);

                String line = null;
                while((line = bufferedReader.readLine()) != null){
                    result += line;
                }

                bufferedReader.close();
            } */

            return jsonObject;
        }

        private LinkedList<Picture> ParseResult(JSONObject jsonObject) throws JSONException{
            LinkedList<Picture> pictures = new LinkedList<Picture>();

            JSONObject jsonObject_responseData = jsonObject.getJSONObject("responseData");
            JSONArray jsonArray_results = jsonObject_responseData.getJSONArray("results");

            Log.d(LOG_TAG,"Result array length => "+jsonArray_results.length());
            for (int i=0;i<jsonArray_results.length();i++) {

                JSONObject jsonObject_i = jsonArray_results.getJSONObject(i);
                String title     = jsonObject_i.getString("title");
                title = new HtmlCleaner().clean(title).getText().toString();
                String content   = jsonObject_i.getString("content");
                content = new HtmlCleaner().clean(content).getText().toString();
                String url       = jsonObject_i.getString("url");

                //Log.d(LOG_TAG,i+": "+title+" "+content+" "+url);

                Picture picture = new Picture((long) MainActivity.internetPictures.size()+i, url, title);
                pictures.add(picture);
            }

            return pictures;
        }

    }

}
