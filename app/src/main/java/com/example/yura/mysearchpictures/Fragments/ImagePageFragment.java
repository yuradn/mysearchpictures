package com.example.yura.mysearchpictures.Fragments;

import android.content.Context;
import android.graphics.Color;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ListView;

import com.actionbarsherlock.app.SherlockFragment;
import com.example.yura.mysearchpictures.R;

public class ImagePageFragment extends SherlockFragment {

    Context context;
    ListView listView;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_image_page, null);

        context = getSherlockActivity().getApplicationContext();
        listView = (ListView) view.findViewById(R.id.listView);


        view.setBackgroundColor(Color.WHITE);

        return view;
    }

    public Context getContext() {
        return context;
    }

    public ListView getListView() {
        return listView;
    }
}






